---
- name: add service group
  group:
    name: splunk
    system: yes
    state: present

- name: add service user
  user:
    name: splunk
    group: splunk
    home: "{{ splunk_home }}"
    create_home: yes
    comment: "splunk system user"
    local: yes
    shell: /bin/sh
    system: yes

- name: get Splunk
  unarchive:
    src: "{{ splunk_archive_url }}"
    dest: /opt
    remote_src: yes

- name: preseed admin user credentials
  blockinfile:
    path: "{{ splunk_home }}/etc/system/local/user-seed.conf"
    state: present
    create: yes
    block: |
      [user_info]
      USERNAME = admin
      PASSWORD = {{ splunk_admin_password }}

- name: stage splunk web.conf
  blockinfile:
    path: "{{ splunk_home }}/etc/system/local/web.conf"
    state: present
    create: yes
    block: |
      [settings]
      server.socket_host = localhost
      mgmtHostPort = {{ ansible_default_ipv4.address }}:8089
      httpport = 8001
      root_endpoint = /splunk
      tools.proxy.on = false

- name: set splunk bind ip 
  lineinfile:
    path: "{{ splunk_home }}/etc/splunk-launch.conf"
    state: present
    create: yes
    line: 'SPLUNK_BINDIP={{ ansible_default_ipv4.address }}'


- name: change '{{ splunk_home }}' file ownership
  file:
    path: "{{ splunk_home }}"
    state: directory
    recurse: yes
    owner: splunk
    group: splunk

- name: Add or modify hard nofile limits for wildcard domain
  community.general.pam_limits:
    domain: '*'
    limit_type: '-'
    limit_item: nofile
    value: 64000

- name: Add or modify hard nofile limits for wildcard domain
  community.general.pam_limits:
    domain: '*'
    limit_type: '-'
    limit_item: nproc
    value: 16000

- name: Add or modify hard nofile limits for wildcard domain
  community.general.pam_limits:
    domain: '*'
    limit_type: '-'
    limit_item: data
    value: 16000000000

- name: Add or modify fsize hard limit
  community.general.pam_limits:
    domain: '*'
    limit_type: '-'
    limit_item: fsize
    value: infinity
    use_max: yes

- name: Add or modify memlock, both soft and hard
  community.general.pam_limits:
    domain: '*'
    limit_type: '-'
    limit_item: memlock
    value: unlimited

# this does not persist, look into a tuned or systemd based method to persist
- name: disable transparent huge pages (not persist)
  shell: "echo never > /sys/kernel/mm/transparent_hugepage/enabled"

- name: Configure firewalld ports for Splunk
  ansible.posix.firewalld:
    port: "{{ item }}"
    permanent: yes
    state: enabled
  with_items:
    - 8089/tcp
    - 8000/tcp
    - 9997/tcp
    - 8080/tcp 

- name: restart firewalld
  systemd:
    name: firewalld
    daemon_reload: yes
    state: restarted

# don't tell Dan Walsh, I'll make a policy
- name: Disable SELinux
  ansible.posix.selinux:
    state: disabled

- name: Start splunk
  shell: "{{ splunk_home }}/bin/splunk start --accept-license --answer-yes"
  become: yes
  become_user: splunk
  # for now
  ignore_errors: yes

- name: Wait for port 8001 to become open on the host, don't start checking for 10 seconds
  wait_for:
    port: 8001
    delay: 10

- name: Stop splunk for systemd configuration
  shell: "{{ splunk_home }}/bin/splunk stop"
  become: yes
  become_user: splunk

- name: Enable the splunkd systemd service 
  shell: "{{ splunk_home }}/bin/splunk enable boot-start -systemd-managed 1 -user splunk -group splunk"

- name: Restart splunk
  systemd:
    name: splunkd
    state: started
