# splunk-installer
A role to download and install Splunk from a .tgz downloaded from splunk.com.  The url used in the download was retrieved from:
https://www.splunk.com/en_us/download/splunk-enterprise.html

Splunk will be configured with a service account named `splunk` which is a member of group `splunk`.

As part of the configuration, Splunk will run as a systemd service named `splunkd`.

A set of ports will be opened on the Splunk host's firewall.  These ports can be found in the Splunk documentation at:

https://docs.splunk.com/Documentation/Splunk/8.1.0/InheritedDeployment/Ports

PAM file limits, fsize, and memlock are set to unlimited (or just really high).  
Transparent huge pages are also disabled, but the method will not persist.  
See Splunk's documentation on why, and choose a method which works for your environment:
https://docs.splunk.com/Documentation/Splunk/8.1.0/ReleaseNotes/SplunkandTHP

Splunk's UI will be behind an NGINX reverse proxy at https://host_or_ip_here/splunk

This was tested against a RHEL 7.9 target and a RHEL 8.3 target.  

Example Playbook:

```
---
- name: Set up Splunk
  hosts: splunk
  roles: 
    - role: splunk-installer
```

Example Inventory:

```
all:
  hosts:
    splunk:
      ansible_host: 192.168.122.220      
```
test
